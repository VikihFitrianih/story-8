from django.urls import path
from .views import books, data

app_name = 'story8'

urlpatterns = [
    path('', books, name='books'),
    path('data/', data, name='data'),
]
